import os.path
import random


def handleUserInputDifficulty():
    '''
    This function asks the user if they would like
    to play the game in (h)ard or (e)asy mode,
    then returns the corresponding number of misses
    allowed for the game.
    '''

    # Your Code Here
    print("How many misses do you want? Hard has 8 and Easy has 12.")
    chchoice = input("(h)ard or (e)asy> ")
    if chchoice == "h":
        print("you have 8 misses to guess the word")
        return 8
    if chchoice == "e":
        print("you have 12 misses to guess the word")
        return 12


def createDisplayString(lettersGuessed, missesLeft, guessedWordAsList):
    '''
    Creates the string that will be displayed to the user, using the information in the parameters.
    '''

    # Your Code Here
    lettersGuessed.sort()
    lettersNotYetGuessed = list("abcdefghijklmnopqrstuvwxyz")
    for letter in lettersGuessed:
        if letter in lettersNotYetGuessed:
            lettersNotYetGuessed[lettersNotYetGuessed.index(letter)] = " "
    firstline = "letters not yet guessed: " + "".join(lettersNotYetGuessed)
    secondline = "misses remaining = " + str(missesLeft)
    thirdline = " ".join(guessedWordAsList)
    return "\n" + firstline + "\n" + secondline + "\n" + thirdline


def handleUserInputLetterGuess(lettersGuessed, displayString):
    '''
    Prints displayString, then asks the user to input a letter to guess.
    This function handles the user input of the new letter guessed and checks if it is a repeated letter.
    '''

    # Your Code Here
    print(displayString)
    chguess = input("letter> ")
    while chguess in lettersGuessed:
        print("\n" + "you already guessed that")
        chguess = input("letter> ")
    return chguess


def handleUserInputDebugMode():
    """
    Prompts the user whether they want to play in debug mode. If user enters
    the letter "d", then debug mode is chosen, and function returns True.
    Else, function returns False.
    """
    userinput = input("Which mode do you want: (d)ebug or (p)lay:> ")
    if userinput == "d":
        return True
    else:
        return False


def handleUserInputWordLength():
    """
    Asks user how long secretWord should be, returning said value (assumption:
    the user will input a value between 5 and 10 (inclusive)).
    """
    userlengthinput = input("How many letters in the word you'll guess:> ")
    return int(userlengthinput)


def createTemplate(currTemplate, letterGuess, word):
    """
    Creates a new template for the secret word that the user will see.
    Returns a new template that is consistent with the previous template and
    word.
    """
    currTemplateLst = list(currTemplate)
    wordlst = list(word)
    if letterGuess in word:
        for i in range(len(currTemplateLst)):
            if wordlst[i] == letterGuess:
                currTemplateLst[i] = letterGuess
            else:
                continue
    return "".join(currTemplateLst)


def getNewWordList(currTemplate, letterGuess, wordList, debug):
    """
    Constructs a dictionary, in which the keys are templates and the values
    are lists of strings that match their respective template. Returns a (
    key, value) pair with the longest list.
    """
    dict = {}
    maxValue = 0
    LargestList = []
    LargestKey = ""
    for word in wordList:
        key = createTemplate(currTemplate, letterGuess, word)
        if key not in dict:
            dict[key] = []
        dict[key].append(word)
    if debug:
        # print("# possible words: " + str(sum([len(v) for v in
        #                                          dict.values()])))
        for (key, value) in sorted(dict.items()):
            print(key + " : " + str(len(value)))
        keycount = sum([1 for k in dict.keys()])
        print("# keys = " + str(keycount))

    for (key, value) in dict.items():
        if len(value) > maxValue:
            maxValue = len(value)
            LargestList = value
            LargestKey = key
        if len(value) == maxValue:
            if LargestKey < key:
                LargestKey = key
                LargestList = value
    return (LargestKey, LargestList)


def processUserGuessClever(guessedLetter, guessedWordAsList, missesLeft):
    """
    Takes user's guess, user's current progress on the word, and the number
    of misses left; updates the number of misses left and indicates whether
    the user missed.
    """
    boolean = ""
    if guessedLetter in guessedWordAsList:
        boolean = True
    elif guessedLetter not in guessedWordAsList:
        print("you missed: " + guessedLetter + " not in word")
        missesLeft = missesLeft - 1
        boolean = False
    return [missesLeft, boolean]


def runGame(filename):
    '''
    This function sets up the game, runs each round, and prints a final message on whether or not the user won.
    True is returned if the user won the game. If the user lost the game, False is returned.
    '''

    # Your Code Here
    file = os.path.join(filename)
    f = open(file)
    words = [w.strip() for w in f.read().split()]

    debug = handleUserInputDebugMode()

    wordLength = handleUserInputWordLength()
    newwords = [w for w in words if len(w) == wordLength]
    secretWord = random.choice(newwords)

    missesLeft = handleUserInputDifficulty()
    totalMisses = missesLeft

    currTemplate = "".join(["_" for num in range(wordLength)])
    lettersGuessed = []
    guessCount = 0

    while missesLeft > 0:

        guessedLetter = handleUserInputLetterGuess(lettersGuessed,
                                                   createDisplayString(
                                                       lettersGuessed,
                                                       missesLeft,
                                                       list(currTemplate)))
        if debug:
            print("(word is " + secretWord + ")")
            print("# possible words: " + str(len(newwords)))
        lettersGuessed += guessedLetter
        guessCount = guessCount + 1
        NewTemplateAndLargestList = getNewWordList(currTemplate, guessedLetter,
                                                   newwords,
                                                   debug)
        currTemplate = NewTemplateAndLargestList[0]
        newwords = NewTemplateAndLargestList[1]
        secretWord = random.choice(NewTemplateAndLargestList[1])
        processUserGuessCleverLst = processUserGuessClever(guessedLetter,
                                                           list(currTemplate),
                                                           missesLeft)
        missesLeft = processUserGuessCleverLst[0]
        if "_" not in list(currTemplate):
            print("you guessed the word: " + secretWord)
            print("you made " + str(guessCount) + " guesses with " +
                  str(totalMisses - missesLeft) + " misses")
            return True
        else:
            continue
    if missesLeft <= 0:
        print("you're hung!!" + "\n" + "word is " + secretWord)
        return False


if __name__ == "__main__":
    pass
    winCount = 0
    lossCount = 0
    while True:
        WinOrLoss = runGame("lowerwords.txt")

        if WinOrLoss == True:
            winCount += 1
        elif WinOrLoss == False:
            lossCount += 1

        YesOrNo = input("Do you want to play again? y or n> ")
        if YesOrNo == "y":
            continue
        elif YesOrNo == "n":
            break
    print("You won " + str(winCount) + " game(s) and lost " + str(lossCount))